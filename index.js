/**
 * @format
 */
import 'react-native-gesture-handler'
import React from 'react'
import { AppRegistry } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { name as appName } from './app.json';
import { Home } from './screens/Home';
import { Details } from './screens/Details';

const { Navigator, Screen } = createStackNavigator()

const App = () => {
  return (
    <NavigationContainer>
      <Navigator initialRouteName='Home'>
        <Screen name='home' component={Home} options={{ title: 'Overview' }} />
        <Screen name='details' component={Details} options={{ title: 'Details' }} />
      </Navigator>
    </NavigationContainer>
  )
}

AppRegistry.registerComponent(appName, () => App);
