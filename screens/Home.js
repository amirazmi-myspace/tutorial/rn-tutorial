import React from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'

export const Home = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>This is home screen</Text>
      <Button title='Go to Details' onPress={() => navigation.navigate('details')} />
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 28
  }
})